#include "input.h"

#include <iostream>
#include <map>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <SFML/Graphics.hpp>

namespace controls {
    using Key = sf::Keyboard::Key;

    // map key strings to sfml key type
    std::map<std::string, sf::Keyboard::Key> keymap = {
            {"A",        Key::A},
            {"B",        Key::B},
            {"C",        Key::C},
            {"D",        Key::D},
            {"E",        Key::E},
            {"F",        Key::F},
            {"G",        Key::G},
            {"H",        Key::H},
            {"I",        Key::I},
            {"J",        Key::J},
            {"K",        Key::K},
            {"L",        Key::L},
            {"M",        Key::M},
            {"N",        Key::N},
            {"O",        Key::O},
            {"P",        Key::P},
            {"Q",        Key::Q},
            {"R",        Key::R},
            {"S",        Key::S},
            {"T",        Key::T},
            {"U",        Key::U},
            {"V",        Key::V},
            {"W",        Key::W},
            {"X",        Key::X},
            {"Y",        Key::Y},
            {"Z",        Key::Z},
            {"0",        Key::Num0},
            {"1",        Key::Num1},
            {"2",        Key::Num2},
            {"3",        Key::Num3},
            {"4",        Key::Num4},
            {"5",        Key::Num5},
            {"6",        Key::Num6},
            {"7",        Key::Num7},
            {"8",        Key::Num8},
            {"9",        Key::Num9},
            {"Left",     Key::Left},
            {"Right",    Key::Right},
            {"Up",       Key::Up},
            {"Down",     Key::Down},
            {"RControl", Key::RControl},
            {"RAlt",     Key::RAlt},
            {"Space",    Key::Space}
    };

    std::map<std::string, int> keycodes = {
            {"k0", 0},
            {"k1", 1},
            {"k2", 2},
            {"k3", 3},
            {"k4", 4},
            {"k5", 5},
            {"k6", 6},
            {"k7", 7},
            {"k8", 8},
            {"k9", 9},
            {"kA", 10},
            {"kB", 11},
            {"kC", 12},
            {"kD", 13},
            {"kE", 14},
            {"kF", 15},
    };

    InputHandler::InputHandler(const std::string &config_file) {
        boost::property_tree::ptree pt;
        boost::property_tree::read_ini(config_file, pt);
        for (const std::pair<std::string, int> k : keycodes) {
            auto key = keymap[pt.get<std::string>("controls." + k.first)];
            m_keypad_keyboard.insert({ keycodes[k.first], key });
            m_keyboard_keypad.insert({ key, keycodes[k.first]});
        }
    }

    int InputHandler::waitKey(sf::RenderWindow &window) {
        sf::Event event;
        while (window.waitEvent(event)) {
            if (event.type == sf::Event::KeyPressed) {
                // if key is valid return the keypad keycode
                if (m_keyboard_keypad.find(event.key.code) != m_keyboard_keypad.end())
                    return m_keyboard_keypad[event.key.code];
            }
        }
        return 0;
    }
}