#ifndef CHIP_8_EMULATOR_CHIP_H
#define CHIP_8_EMULATOR_CHIP_H

#include <iostream>
#include <cstdint>

#include "gfx.h"
#include "sfx.h"

namespace chip8 {
    class INVALID_MEMORY_ACCESS : public std::exception {};
    class STACK_OVERFLOW: public std::exception {};
    class STACK_UNDERFLOW: public std::exception {};

    enum Register {
        R_V0,
        R_V1,
        R_V2,
        R_V3,
        R_V4,
        R_V5,
        R_V6,
        R_V7,
        R_V8,
        R_V9,
        R_VA,
        R_VB,
        R_VC,
        R_VD,
        R_VE,
        R_VF,
        R_ST,  // sound timer
        R_DT,  // delay timer
        R_I
    };


    class Chip {
        static const uint16_t c_mem_size = 0xFFF;
        static const uint16_t c_program_address = 0x200;
        uint8_t m_memory[c_mem_size];
        uint8_t m_registers[18];
        uint16_t m_i_register;
        uint16_t m_pc_register;

        uint16_t m_stack[16];
        int m_sp;
        sfx::Buzzer buzzer;

        void loadHexSprites();  // Store hexadecimal digits 0 through F to memory

        void updateTimers();
    public:
        Chip();

        void reset();

        uint8_t memRead(const uint16_t &address) const;

        void memWrite(uint16_t address, uint8_t value);

        uint16_t getI() const;

        uint8_t regRead(const Register &reg) const;

        void regWrite(const Register &reg, const uint16_t &value);

        void setPC(const uint16_t &address);

        uint16_t getPC() const;

        void pushAddress(const uint16_t &address);

        uint16_t popAddress();

        void skipNextInstruction();

        void loadProgram(std::string filename);

        void execute(gfx::Window &window, const double frequency);
        
        static uint16_t getDigitAddress(int digit) { return digit * 5; }
    };

    inline uint8_t Chip::memRead(const uint16_t &address) const {
        if (address <= c_mem_size && address >= 0)
            return m_memory[address];
        else
            throw INVALID_MEMORY_ACCESS();
    }

    inline void Chip::memWrite(uint16_t address, uint8_t value) {
        if (address <= c_mem_size && address >= 0)
            m_memory[address] = value;
        else
            throw INVALID_MEMORY_ACCESS();
    }

    inline uint16_t Chip::getI() const {
        return m_i_register;
    }

    inline uint8_t Chip::regRead(const Register &reg) const {
        return (reg == R_I) ? m_i_register : m_registers[reg];
    }

    inline void Chip::regWrite(const Register &reg, const uint16_t &value) {
        if (reg == R_I)
            m_i_register = value;
        else
            m_registers[reg] = value;
    }

    inline void Chip::setPC(const uint16_t &address) {
        m_pc_register = address;
    }

    inline uint16_t Chip::getPC() const {
        return m_pc_register;
    }

    inline void Chip::pushAddress(const uint16_t &address) {
        if (m_sp == 15) {
            throw STACK_OVERFLOW();
        }
        else {
            m_stack[++m_sp] = address;
        }
    }

    inline uint16_t Chip::popAddress() {
        if (m_sp == -1) {
            throw STACK_UNDERFLOW();
        }
        else {
            return m_stack[m_sp--];
        }
    }

    inline void Chip::skipNextInstruction() {
        setPC(getPC() + 2);
    }
}

#endif //CHIP_8_EMULATOR_CHIP_H
