#include "chip.h"

#include <fstream>
#include <cstdint>

#include "gfx.h"
#include "sfx.h"
#include "instruction.h"

namespace chip8 {
    Chip::Chip() : buzzer(10000, 170){
        reset();
        loadHexSprites();
    }

    void Chip::reset() {
        for (int i = 0; i < c_mem_size; i++) {
            m_memory[i] = 0;
        }
        for (int i = 0; i < 18; i++) {
            m_registers[i] = 0;
        }
        m_i_register = 0;
        m_pc_register = c_program_address;
        m_sp = -1;
    }

    void Chip::loadProgram(std::string filename) {
        std::ifstream file(filename, std::ios::binary);

        if (!file)
            std::cerr << "Program loading failed" << std::endl;

        file.seekg(0, std::ios::end );
        int fsize = file.tellg();

        file.seekg(0, std::ios::beg);

        char buffer;
        uint16_t address = c_program_address;
        while (fsize) {
            file.read(&buffer, 1);
            m_memory[address] = buffer;
            address++;
            fsize--;
        }

        file.close();
    }

    void Chip::execute(gfx::Window &window, const double frequency) {
        sf::Clock timer_clock;
        sf::Clock freq_clock;
        double ts = (1.0 / frequency) * 1000;

        while (!sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
            int elapsed_time = timer_clock.getElapsedTime().asMilliseconds();
            if (elapsed_time >= 16) {
                timer_clock.restart();
                updateTimers();
            }

            elapsed_time = freq_clock.getElapsedTime().asMilliseconds();
            if (elapsed_time >= ts) {
                freq_clock.restart();
                uint16_t instruction = m_memory[m_pc_register] << 8 | m_memory[m_pc_register + 1];
                m_pc_register += 2;
                isa::executeInstruction(*this, window, instruction);
            }
        }
    }

    void Chip::updateTimers() {
        if (m_registers[R_ST]) {
            buzzer.play();
            --m_registers[R_ST];
        }
        else {
            buzzer.stop();
        }

        if (m_registers[R_DT])
            --m_registers[R_DT];
    }

    void Chip::loadHexSprites() {
        // Store hexadecimal digits 0 through F to memory
        uint16_t address = 0x0;
        // "0"
        memWrite(address++, 0xF0);
        memWrite(address++, 0x90);
        memWrite(address++, 0x90);
        memWrite(address++, 0x90);
        memWrite(address++, 0xF0);

        // "1"
        memWrite(address++, 0x20);
        memWrite(address++, 0x60);
        memWrite(address++, 0x20);
        memWrite(address++, 0x20);
        memWrite(address++, 0x70);

        // "2"
        memWrite(address++, 0xF0);
        memWrite(address++, 0x10);
        memWrite(address++, 0xF0);
        memWrite(address++, 0x80);
        memWrite(address++, 0xF0);

        // "3"
        memWrite(address++, 0xF0);
        memWrite(address++, 0x10);
        memWrite(address++, 0xF0);
        memWrite(address++, 0x10);
        memWrite(address++, 0xF0);

        // "4"
        memWrite(address++, 0x90);
        memWrite(address++, 0x90);
        memWrite(address++, 0xF0);
        memWrite(address++, 0x10);
        memWrite(address++, 0x10);

        // "5"
        memWrite(address++, 0xF0);
        memWrite(address++, 0x80);
        memWrite(address++, 0xF0);
        memWrite(address++, 0x10);
        memWrite(address++, 0xF0);

        // "6"
        memWrite(address++, 0xF0);
        memWrite(address++, 0x80);
        memWrite(address++, 0xF0);
        memWrite(address++, 0x90);
        memWrite(address++, 0xF0);

        // "7"
        memWrite(address++, 0xF0);
        memWrite(address++, 0x10);
        memWrite(address++, 0x20);
        memWrite(address++, 0x40);
        memWrite(address++, 0x40);

        // "8"
        memWrite(address++, 0xF0);
        memWrite(address++, 0x90);
        memWrite(address++, 0xF0);
        memWrite(address++, 0x90);
        memWrite(address++, 0xF0);

        // "9"
        memWrite(address++, 0xF0);
        memWrite(address++, 0x90);
        memWrite(address++, 0xF0);
        memWrite(address++, 0x10);
        memWrite(address++, 0xF0);

        // "A"
        memWrite(address++, 0xF0);
        memWrite(address++, 0x90);
        memWrite(address++, 0xF0);
        memWrite(address++, 0x90);
        memWrite(address++, 0x90);

        // "B"
        memWrite(address++, 0xE0);
        memWrite(address++, 0x90);
        memWrite(address++, 0xE0);
        memWrite(address++, 0x90);
        memWrite(address++, 0xE0);

        // "C"
        memWrite(address++, 0xF0);
        memWrite(address++, 0x80);
        memWrite(address++, 0x80);
        memWrite(address++, 0x80);
        memWrite(address++, 0xF0);

        // "D"
        memWrite(address++, 0xE0);
        memWrite(address++, 0x90);
        memWrite(address++, 0x90);
        memWrite(address++, 0x90);
        memWrite(address++, 0xE0);

        // "E"
        memWrite(address++, 0xF0);
        memWrite(address++, 0x80);
        memWrite(address++, 0xF0);
        memWrite(address++, 0x80);
        memWrite(address++, 0xF0);

        // "F"
        memWrite(address++, 0xF0);
        memWrite(address++, 0x80);
        memWrite(address++, 0xF0);
        memWrite(address++, 0x80);
        memWrite(address++, 0x80);
    }
}
