//#include "instruction.h"

#include <random>

#include "chip.h"
#include "gfx.h"

namespace isa {
    std::default_random_engine rng(std::random_device{}());
    std::uniform_int_distribution<uint8_t> dist(0, 255);  // (min, max)

    /*
     * Instructions
     */

    inline void ins00E0(chip8::Chip &chip, gfx::Window &window) {
        /*
         * 00E0 - CLS
         * Clear the display.
         */
        window.clear();
    }

    inline void ins00EE(chip8::Chip &chip) {
        /*
         * 00EE - RET
         * Return from a subroutine.
         * The interpreter sets the program counter to the address at
         * the top of the stack, then subtracts 1 from the stack pointer.
         */
        chip.setPC(chip.popAddress());
    }

    inline void ins1NNN(chip8::Chip &chip, const uint16_t &nnn) {
        /*
         * 1nnn - JP addr
         * Jump to location nnn.
         */
        chip.setPC(nnn);
    }

    inline void ins2NNN(chip8::Chip &chip, const uint16_t &nnn) {
        /*
         * 2nnn - CALL addr
         * Call subroutine at nnn.
         */
        chip.pushAddress(chip.getPC());
        chip.setPC(nnn);
    }

    inline void insANNN(chip8::Chip &chip, const uint16_t &nnn) {
        /*
         * Annn - LD I, addr
         * The value of register I is set to nnn.
         */
        chip.regWrite(chip8::R_I, nnn);
    }

    inline void insBNNN(chip8::Chip &chip, const uint16_t &nnn) {
        /*
         * Bnnn - JP V0, addr
         * The program counter is set to nnn plus the value of V0.
         */
        chip.setPC(nnn + chip.regRead(chip8::R_V0));
    }

    inline void ins3XKK(chip8::Chip &chip, const chip8::Register &x, const uint8_t &kk) {
        /*
         * 3xkk - SE Vx, byte
         * Skip next instruction if Vx = kk.
         */
        if (chip.regRead(x) == kk)
            chip.skipNextInstruction();
    }

    inline void ins4XKK(chip8::Chip &chip, const chip8::Register &x, const uint8_t &kk) {
        /*
         * 4xkk - SNE Vx, byte
         * Skip next instruction if Vx != kk.
         */
        if (chip.regRead(x) != kk)
            chip.skipNextInstruction();
    }

    inline void ins6XKK(chip8::Chip &chip, const chip8::Register &x, const uint8_t &kk) {
        /*
         * 6xkk - LD Vx, byte
         * Set Vx = kk.
         */
        chip.regWrite(x, kk);
    }

    inline void ins7XKK(chip8::Chip &chip, const chip8::Register &x, const uint8_t &kk) {
        /*
         * 7xkk - ADD Vx, byte
         * Set Vx = Vx + kk.
         */
        chip.regWrite(x, chip.regRead(x) + kk);
    }

    inline void insCXKK(chip8::Chip &chip, const chip8::Register &x, const uint8_t &kk) {
        /*
         * Cxkk - RND Vx, byte
         * Set Vx = random byte AND kk.
         */
        uint8_t rand = dist(rng);
        chip.regWrite(x, rand & kk);
    }

    inline void ins5XY0(chip8::Chip &chip, const chip8::Register &x, const chip8::Register &y) {
        /*
         * 5xy0 - SE Vx, Vy
         * Skip next instruction if Vx = Vy.
         */
        if (chip.regRead(x) == chip.regRead(y))
            chip.skipNextInstruction();
    }

    inline void ins8XY0(chip8::Chip &chip, const chip8::Register &x, const chip8::Register &y) {
        /*
         * 8xy0 - LD Vx, Vy
         * Set Vx = Vy.
         */
        chip.regWrite(x, chip.regRead(y));
    }

    inline void ins8XY1(chip8::Chip &chip, const chip8::Register &x, const chip8::Register &y) {
        /*
         * 8xy1 - OR Vx, Vy
         * Set Vx = Vx OR Vy.
         */
        chip.regWrite(x, chip.regRead(x) | chip.regRead(y));
    }

    inline void ins8XY2(chip8::Chip &chip, const chip8::Register &x, const chip8::Register &y) {
        /*
         * 8xy2 - AND Vx, Vy
         * Set Vx = Vx AND Vy.
         */
        chip.regWrite(x, chip.regRead(x) & chip.regRead(y));
    }

    inline void ins8XY3(chip8::Chip &chip, const chip8::Register &x, const chip8::Register &y) {
        /*
         * 8xy3 - XOR Vx, Vy
         * Set Vx = Vx XOR Vy.
         */
        chip.regWrite(x, chip.regRead(x) ^ chip.regRead(y));
    }

    inline void ins8XY4(chip8::Chip &chip, const chip8::Register &x, const chip8::Register &y) {
        /*
         * 8xy4 - ADD Vx, Vy
         * Set Vx = Vx + Vy, set VF = carry.
         */
        uint8_t vx = chip.regRead(x);
        uint8_t vy = chip.regRead(y);
        chip.regWrite(x, vx + vy);

        uint8_t carry = (vx > (UINT8_MAX - vy) || vy > (UINT8_MAX - vx)) ? 1 : 0;
        chip.regWrite(chip8::R_VF, carry);
    }

    inline void ins8XY5(chip8::Chip &chip, const chip8::Register &x, const chip8::Register &y) {
        /*
         * 8xy5 - SUB Vx, Vy
         * Set Vx = Vx - Vy, set VF = NOT borrow.
         * If Vx > Vy, then VF is set to 1, otherwise 0. Then Vy is subtracted
         * from Vx, and the results stored in Vx.
         */
        uint8_t vx = chip.regRead(x);
        uint8_t vy = chip.regRead(y);
        uint8_t b = (vx > vy) ? 1 : 0;

        chip.regWrite(chip8::R_VF, b);
        chip.regWrite(x, vx - vy);
    }

    inline void ins8XY6(chip8::Chip &chip, const chip8::Register &x, const chip8::Register &y) {
        /*
         * 8xy6 - SHR Vx {, Vy}
         * Set Vx = Vx SHR 1.
         * If the least-significant bit of Vx is 1, then VF is set to 1,
         * otherwise 0. Then Vx is divided by 2.
         */
        uint8_t vx = chip.regRead(x);
        uint8_t c = ((vx & 0x01) == 1) ? 1 : 0;

        chip.regWrite(chip8::R_VF, c);
        chip.regWrite(x, vx >> 1);
    }

    inline void ins8XY7(chip8::Chip &chip, const chip8::Register &x, const chip8::Register &y) {
        /*
         * 8xy7 - SUBN Vx, Vy
         * Set Vx = Vy - Vx, set VF = NOT borrow.
         * If Vy > Vx, then VF is set to 1, otherwise 0. Then Vx is subtracted
         * from Vy, and the results stored in Vx.
         */
        uint8_t vx = chip.regRead(x);
        uint8_t vy = chip.regRead(y);
        uint8_t b = (vy > vx) ? 1 : 0;

        chip.regWrite(chip8::R_VF, b);
        chip.regWrite(x, vy - vx);
    }

    inline void ins8XYE(chip8::Chip &chip, const chip8::Register &x, const chip8::Register &y) {
        /*
         * 8xyE - SHL Vx {, Vy}
         * Set Vx = Vx SHL 1.
         * If the most-significant bit of Vx is 1, then VF is set to 1,
         * otherwise to 0. Then Vx is multiplied by 2.
         */
        uint8_t vx = chip.regRead(x);
        uint8_t c = ((vx & 0x80) == 0x80) ? 1 : 0;

        chip.regWrite(chip8::R_VF, c);
        chip.regWrite(x, vx << 1);
    }

    inline void ins9XY0(chip8::Chip &chip, const chip8::Register &x, const chip8::Register &y) {
        /*
         * 9xy0 - SNE Vx, Vy
         * Skip next instruction if Vx != Vy.
         */
        if (chip.regRead(x) != chip.regRead(y))
            chip.skipNextInstruction();
    }

    inline void insDXYN(chip8::Chip &chip, gfx::Window &window,
                        const chip8::Register &x, const chip8::Register &y,
                        const uint8_t n) {
        /*
         * Dxyn - DRW Vx, Vy, nibble
         * Display n-byte sprite starting at memory location I at (Vx, Vy),
         * set VF = collision.
         */
        uint8_t coord_x = chip.regRead(x);
        uint8_t coord_y = chip.regRead(y);
        uint16_t address = chip.getI();
        bool collision = false;

        for (int i = 0; i < n; i++) {
            collision |= window.draw(coord_x, coord_y++, chip.memRead(address++));
        }

        if (collision)
            chip.regWrite(chip8::R_VF, 1);
        else
            chip.regWrite(chip8::R_VF, 0);
    }

    inline void insEX9E(chip8::Chip &chip, gfx::Window &window, const chip8::Register &x) {
        /*
         * Ex9E - SKP Vx
         * Skip next instruction if key with the value of Vx is pressed.
         */
        if (window.isKeyPressed(chip.regRead(x)))
            chip.skipNextInstruction();
    }

    inline void insEXA1(chip8::Chip &chip, gfx::Window &window, const chip8::Register &x) {
        /*
         * ExA1 - SKNP Vx
         * Skip next instruction if key with the value of Vx is not pressed.
         */
        if (!window.isKeyPressed(chip.regRead(x)))
            chip.skipNextInstruction();
    }

    inline void insFX07(chip8::Chip &chip, const chip8::Register &x) {
        /*
         * Fx07 - LD Vx, DT
         * Set Vx = delay timer value.
         */
        chip.regWrite(x, chip.regRead(chip8::R_DT));
    }

    inline void insFX0A(chip8::Chip &chip, gfx::Window &window, const chip8::Register &x) {
        /*
         * Fx0A - LD Vx, K
         * Wait for a key press, store the value of the key in Vx.
         */
        uint8_t key = window.waitKey();
        chip.regWrite(x, key);
    }

    inline void insFX15(chip8::Chip &chip, const chip8::Register &x) {
        /*
         * Fx15 - LD DT, Vx
         * Set delay timer = Vx.
         */
        chip.regWrite(chip8::R_DT, chip.regRead(x));
    }

    inline void insFX18(chip8::Chip &chip, const chip8::Register &x) {
        /*
         * Fx18 - LD ST, Vx
         * Set sound timer = Vx.
         */
        chip.regWrite(chip8::R_ST, chip.regRead(x));
    }

    inline void insFX1E(chip8::Chip &chip, const chip8::Register &x) {
        /*
         * Fx1E - ADD I, Vx
         * Set I = I + Vx.
         */
        uint16_t vx = chip.regRead(x);
        uint16_t vi = chip.getI();
        chip.regWrite(chip8::R_I, vi + vx);
    }

    inline void insFX29(chip8::Chip &chip, const chip8::Register &x) {
        /*
         * Fx29 - LD F, Vx
         * Set I = location of sprite for digit Vx.
         */
        chip.regWrite(chip8::R_I, chip8::Chip::getDigitAddress(chip.regRead(x)));
    }

    inline void insFX33(chip8::Chip &chip, const chip8::Register &x) {
        /*
         * Fx33 - LD B, Vx
         * Store BCD representation of Vx in memory locations I, I+1, and I+2.
         */
        uint8_t vx = chip.regRead(x);
        uint16_t address = chip.getI();
        chip.memWrite(address++, vx / 100);
        chip.memWrite(address++, (vx / 10) % 10);
        chip.memWrite(address, vx % 10);
    }

    inline void insFX55(chip8::Chip &chip, const chip8::Register &x) {
        /*
         * Fx55 - LD [I], Vx
         * Store registers V0 through Vx in memory starting at location I.
         */
        uint16_t address = chip.getI();
        for (uint8_t reg = 0; reg <= x; reg++) {
            chip.memWrite(address++, chip.regRead(chip8::Register(reg)));
        }
    }

    inline void insFX65(chip8::Chip &chip, const chip8::Register &x) {
        /*
         * Fx65 - LD Vx, [I]
         * Read registers V0 through Vx from memory starting at location I.
         */
        uint16_t address = chip.getI();
        for (uint8_t reg = 0; reg <= x; reg++) {
            chip.regWrite(chip8::Register(reg), chip.memRead(address++));
        }
    }

    /*
     * Instruction Types
     */

    void op0EF(chip8::Chip &chip, gfx::Window &window, const uint16_t &instruction) {
        uint8_t f = instruction & 0x000F;

        switch (f) {
            case 0x0: { ins00E0(chip, window); break; }
            case 0xE: { ins00EE(chip); break; }
        }
    }

    void opNNN(chip8::Chip &chip, gfx::Window &window, const uint16_t &instruction) {
        uint8_t op = (instruction & 0xF000) >> 12;
        uint16_t nnn = instruction & 0x0FFF;

        switch (op) {
            case 0x1: { ins1NNN(chip, nnn); break; }
            case 0x2: { ins2NNN(chip, nnn); break; }
            case 0xA: { insANNN(chip, nnn); break; }
            case 0xB: { insBNNN(chip, nnn); break; }
        }
    }

    void opXKK(chip8::Chip &chip, gfx::Window &window, const uint16_t &instruction) {
        uint8_t op = (instruction & 0xF000) >> 12;
        uint8_t x = (instruction & 0x0F00) >> 8;
        uint8_t kk = instruction & 0x00FF;
        auto reg_x = chip8::Register(x);

        switch (op) {
            case 0x3: { ins3XKK(chip, reg_x, kk); break; }
            case 0x4: { ins4XKK(chip, reg_x, kk); break; }
            case 0x6: { ins6XKK(chip, reg_x, kk); break; }
            case 0x7: { ins7XKK(chip, reg_x, kk); break; }
            case 0xC: { insCXKK(chip, reg_x, kk); break; }
        }
    }

    void opXYF(chip8::Chip &chip, gfx::Window &window, const uint16_t &instruction) {
        uint8_t op = (instruction & 0xF000) >> 12;
        uint8_t x = (instruction & 0x0F00) >> 8;
        uint8_t y = (instruction & 0x00F0) >> 4;
        uint8_t f = instruction & 0x000F;
        auto reg_x = chip8::Register(x);
        auto reg_y = chip8::Register(y);

        switch (op) {
            case 0x5: { ins5XY0(chip, reg_x, reg_y); break; }
            case 0x8: {
                switch (f) {
                    case 0x0: { ins8XY0(chip, reg_x, reg_y); break; }
                    case 0x1: { ins8XY1(chip, reg_x, reg_y); break; }
                    case 0x2: { ins8XY2(chip, reg_x, reg_y); break; }
                    case 0x3: { ins8XY3(chip, reg_x, reg_y); break; }
                    case 0x4: { ins8XY4(chip, reg_x, reg_y); break; }
                    case 0x5: { ins8XY5(chip, reg_x, reg_y); break; }
                    case 0x6: { ins8XY6(chip, reg_x, reg_y); break; }
                    case 0x7: { ins8XY7(chip, reg_x, reg_y); break; }
                    case 0xE: { ins8XYE(chip, reg_x, reg_y); break; }
                }
                break;
            }
            case 0x9: { ins9XY0(chip, reg_x, reg_y); break; }
            case 0xD: { insDXYN(chip, window, reg_x, reg_y, f); break; }
        }
    }

    void opXFF(chip8::Chip &chip, gfx::Window &window, const uint16_t &instruction) {
        uint8_t op = (instruction & 0xF000) >> 12;
        uint8_t x = (instruction & 0x0F00) >> 8;
        uint8_t ff = instruction & 0x00FF;
        auto reg_x = chip8::Register(x);

        switch (op) {
            case 0xE: {
                switch (ff) {
                    case 0x9E: { insEX9E(chip, window, reg_x); break; }
                    case 0xA1: { insEXA1(chip, window, reg_x); break; }
                }
                break;
            }
            case 0xF: {
                switch (ff) {
                    case 0x07: { insFX07(chip, reg_x); break; }
                    case 0x0A: { insFX0A(chip, window, reg_x); break; }
                    case 0x15: { insFX15(chip, reg_x); break; }
                    case 0x18: { insFX18(chip, reg_x); break; }
                    case 0x1E: { insFX1E(chip, reg_x); break; }
                    case 0x29: { insFX29(chip, reg_x); break; }
                    case 0x33: { insFX33(chip, reg_x); break; }
                    case 0x55: { insFX55(chip, reg_x); break; }
                    case 0x65: { insFX65(chip, reg_x); break; }
                }
                break;
            }
        }
    }

    void (*op_table[])(chip8::Chip &, gfx::Window &window, const uint16_t &) = {
            op0EF, opNNN, opNNN, opXKK, opXKK, opXYF, opXKK, opXKK,
            opXYF, opXYF, opNNN, opNNN, opXKK, opXYF, opXFF, opXFF
    };

    void executeInstruction(chip8::Chip &chip,
                            gfx::Window &window,
                            const uint16_t &instruction) {
        uint8_t op = (instruction & 0xF000) >> 12;
        op_table[op](chip, window, instruction);
    }
}
