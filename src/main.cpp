#include <string>

#include "chip.h"
#include "gfx.h"

int main(int argc, char *argv[]) {
    chip8::Chip chip;
    gfx::Window window(20);

    std::string rom_filename(argv[1]);
    chip.loadProgram(rom_filename);
    chip.execute(window, 500);

    return 0;
}
