#ifndef CHIP_8_EMULATOR_INPUT_H
#define CHIP_8_EMULATOR_INPUT_H

#include <iostream>
#include <map>
#include <vector>

#include <SFML/Graphics.hpp>

namespace controls {
    class InputHandler {
        std::map<int, sf::Keyboard::Key> m_keypad_keyboard;
        std::map<sf::Keyboard::Key, int> m_keyboard_keypad;
    public:
        InputHandler(const std::string &config_file = "../config.ini");

        bool isKeyPressed(int key);

        int waitKey(sf::RenderWindow &window);
    };

    inline bool InputHandler::isKeyPressed(int key) {
        return sf::Keyboard::isKeyPressed(m_keypad_keyboard[key]);
    }
}

#endif //CHIP_8_EMULATOR_INPUT_H
