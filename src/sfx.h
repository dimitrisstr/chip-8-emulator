#ifndef CHIP_8_EMULATOR_SFX_H
#define CHIP_8_EMULATOR_SFX_H

#include <SFML/Audio.hpp>

namespace sfx {
    class Buzzer {
        static const int c_nsamples = 44100;
        bool m_playing;
        sf::SoundBuffer m_buffer;
        sf::Sound m_sound;
    public:
        Buzzer(int amplitude, int frequency);

        void play();

        void stop();
    };

    inline void Buzzer::play() {
        if (!m_playing) {
            m_sound.play();
            m_playing = true;
        }
    }

    inline void Buzzer::stop() {
        m_sound.stop();
        m_playing = false;
    }
}

#endif //CHIP_8_EMULATOR_SFX_H
