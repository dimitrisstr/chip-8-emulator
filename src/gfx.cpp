#include "gfx.h"

#include <iostream>

#include <cstdio>

#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Image.hpp>

namespace gfx {
    const sf::Color black(0, 0, 0);
    const sf::Color white(255, 255, 255);

    Window::Window(const int &scale, const std::string &title)
    : m_input_handler() {
        m_screen = new sf::RectangleShape*[c_rows];
        for (int i = 0; i < c_rows; i++) {
            m_screen[i] = new sf::RectangleShape[c_columns];
            for (int j = 0; j < c_columns; j++) {
                m_screen[i][j].setSize(sf::Vector2f(scale, scale));
                m_screen[i][j].setPosition(j * scale, i * scale);
                m_screen[i][j].setFillColor(black);
            }
        }

        m_window.create(sf::VideoMode(c_columns * scale,
                                      c_rows * scale), title);
        render();
    }

    Window::~Window() {
        delete[] m_screen;
        m_window.close();
    }

    bool Window::draw(uint8_t x, uint8_t y, uint8_t pixels) {
        uint8_t bit;
        bool collision = false;
        for (int i = 0; i < 8; i++) {
            bit = pixels & 0x80;  // store the first bit
            pixels <<= 1;
            if (bit != 0)
                collision |= setPixel(x + i, y);
        }
        render();
        return collision;
    }

    void Window::render() {
        for (int i = 0; i < c_rows; i++) {
            for (int j = 0; j < c_columns; j++) {
                m_window.draw(m_screen[i][j]);
            }
        }
        m_window.display();
    }

    void Window::clear() {
        for (int i = 0; i < c_rows; i++) {
            for (int j = 0; j < c_columns; j++) {
                m_screen[i][j].setFillColor(black);
            }
        }
        render();
    }

    bool Window::setPixel(uint8_t x, uint8_t y) {
        if (x < c_columns && y < c_rows) {
            bool collision = m_screen[y][x].getFillColor() == white;
            if (collision)
                m_screen[y][x].setFillColor(black);
            else
                m_screen[y][x].setFillColor(white);
            return collision;
        }
        return false;
    }
}
