#ifndef CHIP_8_EMULATOR_INSTRUCTION_H
#define CHIP_8_EMULATOR_INSTRUCTION_H

#include <iostream>

#include "chip.h"
#include "gfx.h"

// instruction set architecture
namespace isa {
    void executeInstruction(chip8::Chip &chip,
                            gfx::Window &window,
                            const uint16_t &instruction);
}

#endif //CHIP_8_EMULATOR_INSTRUCTION_H
