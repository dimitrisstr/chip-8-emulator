#ifndef CHIP_8_EMULATOR_GFX_H
#define CHIP_8_EMULATOR_GFX_H

#include <iostream>

#include <SFML/Graphics.hpp>

#include "input.h"

namespace gfx {
    class Window {

        static const int c_rows = 32;
        static const int c_columns = 64;
        sf::RenderWindow m_window;
        sf::RectangleShape **m_screen;
        controls::InputHandler m_input_handler;

        bool setPixel(uint8_t x, uint8_t y);

        void render();

    public:

        Window(const int &scale, const std::string &title="Chip8 Emulator");

        ~Window();

        bool isKeyPressed(int key);

        int waitKey();

        bool draw(uint8_t x, uint8_t y, uint8_t pixels);

        void clear();
    };

    inline bool Window::isKeyPressed(int key) {
        return m_input_handler.isKeyPressed(key);
    }

    inline int Window::waitKey() {
        return m_input_handler.waitKey(m_window);
    }
}

#endif //CHIP_8_EMULATOR_GFX_H
