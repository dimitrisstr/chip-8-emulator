#include "sfx.h"

#include <cmath>
#include <cstdint>
#include <SFML/Audio.hpp>

namespace sfx {
    Buzzer::Buzzer(int amplitude, int frequency) {
        int16_t samples[c_nsamples];
        double w = 2 * M_PI * frequency;
        for (int i = 0; i < c_nsamples; i++) {
            samples[i] = amplitude * sin(w * (double(i) / c_nsamples));
        }

        m_buffer.loadFromSamples(samples, c_nsamples, 2, c_nsamples);
        m_sound.setBuffer(m_buffer);
        m_sound.setLoop(true);
        m_playing = false;
    }
}